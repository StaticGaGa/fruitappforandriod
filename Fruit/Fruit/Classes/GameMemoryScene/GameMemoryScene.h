//
//  GameMemoryScene.h
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#ifndef __Fruit__GameMemoryScene__
#define __Fruit__GameMemoryScene__

#include <iostream>
#include "cocos2d.h"
#include "FruitSprite.h"

class GameMemoryScene: public cocos2d::CCLayer
{
public:
    static cocos2d::CCScene *scene();
    CREATE_FUNC(GameMemoryScene);
    bool init();
    void dialogueShow(CCNode *pSender);
    void goNext(CCObject *pObject);
    cocos2d::CCPoint getPointFromFramePoint(int tmpFramePoint);
};
#endif /* defined(__Fruit__GameMemoryScene__) */
