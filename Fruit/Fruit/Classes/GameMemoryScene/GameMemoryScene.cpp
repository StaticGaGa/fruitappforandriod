//
//  GameMemoryScene.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#include "GameMemoryScene.h"
#include "CCFruitsData.h"
#include "GamePlayScene.h"

#define Background "Memory_Background.png"
#define Frame "Memory_Frame.png"
#define Touch_Menu "Memory_Touch_Menu.png"
#define Sprite "guaidanshou.png"
#define Cue "tishi1.png"
#define Cue_Two "tishi2.png"

#define Sprite_Tag 1
#define FrameSprite_Tag 2
#define TouchMenu_Tag 3
#define DialogueSprite_Tag 4
#define Move_Time 0.8


using namespace cocos2d;

CCScene *GameMemoryScene::scene()
{
    CCScene *scene = CCScene::create();
    GameMemoryScene *layer = GameMemoryScene::create();
    scene->addChild(layer);
    return scene;
}

bool GameMemoryScene::init()
{
    if (!CCLayer::init()) {
        return false;
    }
    
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    CCSprite *bg = CCSprite::create(Background);
    bg->setPosition(ccp(winSize.width/2, winSize.height/2));
    bg->setTag(Sprite_Tag);
    this->addChild(bg);

    CCSprite *danSprite = CCSprite::create(Sprite);
    CCSize cttSize = danSprite->getContentSize();
    danSprite->setPosition(ccp(-cttSize.width/2, cttSize.height/2));
    this->addChild(danSprite);
  
    CCMoveBy *moveAct2 = CCMoveBy::create(Move_Time, ccp(cttSize.width*10/9 + 30, 0));
    CCMoveBy *moveAct = CCMoveBy::create(0.1, ccp(-30, 0));
    CCCallFuncN *callBack = CCCallFuncN::create(this, callfuncN_selector(GameMemoryScene::dialogueShow));
    danSprite->runAction(CCSequence::create(moveAct2,moveAct,callBack,NULL));
    
    CCSprite *frameSprite = CCSprite::create(Frame);
    cttSize = frameSprite->getContentSize();
    frameSprite->setPosition(ccp(winSize.width + cttSize.width/2, cttSize.height/2));
    frameSprite->setTag(FrameSprite_Tag);
    this->addChild(frameSprite);
   
    CCSprite *nextMenuNormal = CCSprite::create(Touch_Menu);
    CCMenuItemSprite *nextMenu = CCMenuItemSprite::create(nextMenuNormal, NULL, this, menu_selector(GameMemoryScene::goNext));
    nextMenu->setPosition(ccp(cttSize.width/5*4, cttSize.height/4));
    CCMenu *menu = CCMenu::create(nextMenu,NULL);
    menu->setPosition(CCPointZero);
    frameSprite->addChild(menu);
    
    CCFruitsData *fruits = CCFruitsData::sharedFruits();
    fruits->fruitsRange();
    CCArray *array = fruits->getFruits();
    int count = array->count();

    for (int i = 0 ; i < count; i++) {
        FruitSprite *sprite = (FruitSprite *)array->objectAtIndex(i);
        sprite->setPosition(this->getPointFromFramePoint(sprite->getFramePoint()));
        frameSprite->addChild(sprite);
    }

    moveAct = CCMoveBy::create(0.1, ccp(30, 0));
    moveAct2 = CCMoveBy::create(Move_Time, ccp(-cttSize.width - 30, 0));
    frameSprite->runAction(CCSequence::create(moveAct2,moveAct,NULL));

    return true;
}

void GameMemoryScene::dialogueShow(cocos2d::CCNode *pSender)
{
    CCSprite *danSprite = (CCSprite *)pSender;
    CCSprite *dialogueSprite = CCSprite::create(Cue);
    dialogueSprite->setPosition(ccp(dialogueSprite->getContentSize().width/2, danSprite->getContentSize().height + dialogueSprite->getContentSize().height/2));
    this->addChild(dialogueSprite);
}

void GameMemoryScene::goNext(cocos2d::CCObject *pObject)
{
    CCDirector::sharedDirector()->replaceScene(GamePlayScene::scene());
}

CCPoint GameMemoryScene::getPointFromFramePoint(int tmpFramePoint)
{
    CCSprite *frameSprite = (CCSprite *)this->getChildByTag(FrameSprite_Tag);
    CCSize cttSize = frameSprite->getContentSize();
    CCPoint point;
    switch (tmpFramePoint) {
        case 0:
        {
            point = ccp(cttSize.width/20*5, cttSize.height/80*16);
        }
            break;
        case 1:
        {
            point = CCPointMake(cttSize.width/20*8, cttSize.height/80*16);

        }
            break;
        case 2:
        {
            point = CCPointMake(cttSize.width/20*11, cttSize.height/80*16);

        }
            break;
        case 3:
        {
            point = CCPointMake(cttSize.width/20*5, cttSize.height/80*29);
        }
            break;
        case 4:
        {
            point = CCPointMake(cttSize.width/20*8, cttSize.height/80*29);

        }
            break;
        case 5:
        {
            point = CCPointMake(cttSize.width/20*11, cttSize.height/80*29);

        }
            break;
        case 6:
        {
            point = CCPointMake(cttSize.width/20*5, cttSize.height/80*42);

        }
            break;
        case 7:
        {
            point = CCPointMake(cttSize.width/20*8, cttSize.height/80*42);

        }
            break;
        case 8:
        {
            point = CCPointMake(cttSize.width/20*11, cttSize.height/80*42);

        }
            break;
        case 9:
        {
            point = CCPointMake(cttSize.width/20*5, cttSize.height/80*54);

        }
            break;
        case 10:
        {
            point = CCPointMake(cttSize.width/20*8, cttSize.height/80*54);

        }
            break;
        case 11:
        {
            point = CCPointMake(cttSize.width/20*11, cttSize.height/80*54);

        }
            break;
        case 12:
        {
            point = CCPointMake(cttSize.width/20*5, cttSize.height/80*67);

        }
            break;
        case 13:
        {
            point = CCPointMake(cttSize.width/20*8, cttSize.height/80*67);

        }
            break;
        case 14:
        {
            point = CCPointMake(cttSize.width/20*11, cttSize.height/80*67);

        }
            break;
        default:
            break;
    }
    return point;
}



