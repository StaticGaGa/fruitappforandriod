//
//  GameLoginAwardsScene.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-21.
//
//

#include "GameLoginAwardsScene.h"
#include "CCFruitsData.h"
#include "CCPlistData.h"

#define BG "commonBG.png"
#define Add1 "add1.png"
#define Add3 "add3.png"
#define Add5 "add5.png"
#define Guess "guess.png"
#define LoginIcon "loginawards.png"
#define Back "fanhui.png"
using namespace cocos2d;

bool GameLoginAwardsScene::init() {
    if (!CCLayer::init()) {
        return false;
    }
    itemsArray = new CCArray();
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    CCSprite *bg = CCSprite::create(BG);
    bg->setPosition(ccp(winSize.width/2, winSize.height/2));
    this->addChild(bg);
    
    CCSprite *loginSprite = CCSprite::create(LoginIcon);
    loginSprite->setPosition(ccp(winSize.width/2, bg->getContentSize().height - loginSprite->getContentSize().height/2));
    bg->addChild(loginSprite);
    
    CCSprite *normalSprite = CCSprite::create(Guess);
    CCMenuItemSprite *guessItem = CCMenuItemSprite::create(normalSprite, NULL, this, menu_selector(GameLoginAwardsScene::toggleTheItem));
    guessItem->setTag(1001);
    float centerX = guessItem->getContentSize().width;
    float centerY = guessItem->getContentSize().height;
    
    CCMenu *menu = CCMenu::create(NULL);
    
    for (int i = 0; i < 3;i++) {
        guessItem->setPosition(ccp(centerX, centerY));
        centerX += guessItem->getContentSize().width*1.3;
        centerY = guessItem->getContentSize().height;
        menu->addChild(guessItem);
        itemsArray->addObject(guessItem);
        normalSprite = CCSprite::create(Guess);
        guessItem = CCMenuItemSprite::create(normalSprite, NULL, this, menu_selector(GameLoginAwardsScene::toggleTheItem));
        guessItem->setTag(1002 + i);
    }
    normalSprite = CCSprite::create(Back);
    CCMenuItemSprite *backItem = CCMenuItemSprite::create(normalSprite, NULL, this,menu_selector(GameLoginAwardsScene::toggleTheItem));
    backItem->setPosition(ccp(bg->getContentSize().width - backItem->getContentSize().width, backItem->getContentSize().height));
    backItem->setTag(2000);
    menu->addChild(backItem);
    menu->setPosition(CCPointZero);
    bg->addChild(menu);
    
    return true;
}

void GameLoginAwardsScene::toggleTheItem(cocos2d::CCObject *pObj) {
    if (pObj) {
        
        for (int i = 0; i < itemsArray->count();i++) {
            CCMenuItemSprite *item = (CCMenuItemSprite *)itemsArray->objectAtIndex(i);
            item->setEnabled(false);
        }
        
        CCMenuItemSprite *pNode = (CCMenuItemSprite *)pObj;
        int tag = pNode->getTag();
        if (2000 == tag) {
            this->removeFromParentAndCleanup(true);
            return;
        }
        int x = arc4random()%3;
        int djNum = CCPlistData::getDJNum();
        switch (x) {
            case 0:
            {
                pNode->setNormalImage(CCSprite::create(Add1));
                CCPlistData::saveDJNum(djNum + 1);
            }
                break;
            case 1:
            {
                pNode->setNormalImage(CCSprite::create(Add3));
                CCPlistData::saveDJNum(djNum + 3);

            }
                break;
            case 2:
            {
                pNode->setNormalImage(CCSprite::create(Add5));
                CCPlistData::saveDJNum(djNum + 5);

            }
                break;
            default:
                break;
        }
        CCCallFunc *callAct = CCCallFunc::create(this, callfunc_selector(GameLoginAwardsScene::callBack));
        CCDelayTime *delay = CCDelayTime::create(3);
        this->runAction(CCSequence::create(delay,callAct,NULL));
    }
}

void GameLoginAwardsScene::callBack() {
    this->removeFromParentAndCleanup(true);
}

