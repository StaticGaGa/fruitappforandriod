//
//  GameLoginAwardsScene.h
//  Fruit
//
//  Created by Static Ga on 13-5-21.
//
//

#ifndef __Fruit__GameLoginAwardsScene__
#define __Fruit__GameLoginAwardsScene__

#include <iostream>
#include "cocos2d.h"
class GameLoginAwardsScene: public cocos2d::CCLayer {
public:
    ~GameLoginAwardsScene() {
        CC_SAFE_RELEASE(itemsArray);
    }
    CREATE_FUNC(GameLoginAwardsScene);
    bool init();
    void toggleTheItem(cocos2d::CCObject *pObj);
private:
    cocos2d::CCArray *itemsArray;
    void callBack();
};
#endif /* defined(__Fruit__GameLoginAwardsScene__) */
