//
//  HelpNumSprite.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#include "HelpNumSprite.h"
#include "CCPlistData.h"

using namespace cocos2d;


HelpNumSprite *HelpNumSprite::create() {
    return HelpNumSprite::createWithNum(CCPlistData::getDJNum());
}

HelpNumSprite *HelpNumSprite::create(const char *fileName) {
    HelpNumSprite *sprite = new HelpNumSprite();
    if (sprite && sprite->initWithFile(fileName)) {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

HelpNumSprite *HelpNumSprite::createWithNum(int num) {
    HelpNumSprite *sprite = HelpNumSprite::create("fangdajingjiaobiao.png");
    sprite->addNum(num);
    return sprite;
}

void HelpNumSprite::addNum(int num) {
    CCSize cttSize = this->getContentSize();
    CCString *numStr = CCString::createWithFormat("%d",num);
    int tmpNum = num > 0? 0 : 1;
    while (num) {
        num /= 10;
        tmpNum++;
    }
    
    CCLabelTTF *numLable = CCLabelTTF::create(numStr->getCString(), "Thonburi", cttSize.width/tmpNum);
    numLable->setPosition(ccp(cttSize.width/2, cttSize.height/2));
    this->addChild(numLable);
}