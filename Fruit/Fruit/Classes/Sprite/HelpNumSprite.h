//
//  HelpNumSprite.h
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#ifndef __Fruit__HelpNumSprite__
#define __Fruit__HelpNumSprite__

#include <iostream>
#include "cocos2d.h"

class HelpNumSprite: public cocos2d::CCSprite {
public:
    static HelpNumSprite *create();
    static HelpNumSprite *create(const char *fileName);
    void addNum(int num);
    static HelpNumSprite *createWithNum(int num);
};
#endif /* defined(__Fruit__HelpNumSprite__) */
