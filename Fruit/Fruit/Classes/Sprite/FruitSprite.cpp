//
//  FruitSprite.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#include "FruitSprite.h"

FruitSprite *FruitSprite::create(const char *fileName)
{
    FruitSprite *sprite = new FruitSprite();
    if (sprite && sprite->initWithFile(fileName)) {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}