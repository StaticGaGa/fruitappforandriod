//
//  FruitSprite.h
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#ifndef __Fruit__FruitSprite__
#define __Fruit__FruitSprite__

#include <iostream>
#include "cocos2d.h"
class FruitSprite: public cocos2d::CCSprite
{
public:
    enum {
        Point0 = 0,
        Point1 = 1,
        Point2 = 2,
        Point3 = 3,
        Point4 = 4,
        Point5 = 5,
        Point6 = 6,
        Point7 = 7,
        Point8 = 8,
        Point9 = 9,
        Point10 = 10,
        Point11 = 11,
        Point12 = 12,
        Point13 = 13,
        Point14 = 14
    };
private:
    int framePoint;
public:
    static FruitSprite *create(const char *fileName);
    int getFramePoint(){return framePoint;}
    void setFramePoint(int tmpPoint){framePoint = tmpPoint;}
};
#endif /* defined(__Fruit__FruitSprite__) */
