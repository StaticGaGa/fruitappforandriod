//
//  GameShopScene.h
//  Fruit
//
//  Created by Static Ga on 13-5-9.
//
//

#ifndef __Fruit__GameShopScene__
#define __Fruit__GameShopScene__

#include <iostream>
#include "cocos2d.h"

class GameShopScene: public cocos2d::CCLayer
{
public:
    typedef enum  {
        kBackMainMenuType = 0,
        kBackPlayMenuType ,
        kBackResultMenuType,
    }kBackMenuType;
    
    typedef enum {
        IAP6 = 10,
        IAP12 ,
        IAP18,
    }kBuyTag;
    
    int buyType;
    kBackMenuType backType;
public:
    bool init(kBackMenuType type);
    static cocos2d::CCScene *sceneWithType(kBackMenuType type);
    
    void setResultType(int tmpType){resultType =  tmpType;}
    
private:
    void payfor(CCNode *pSender);
    void back(CCNode *pSender);
    int resultType;
};
#endif /* defined(__Fruit__GameShopScene__) */
