//
//  GameShopScene.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-9.
//
//

#include "GameShopScene.h"
#include "HelloWorldScene.h"
#include "GamePlayScene.h"
#include "GameResultScene.h"

#define GS_BG "shangdiantongyong.png"
#define GS_BackNormal "fanhui.png"
#define GS_BackPressed "fanhuiPressed.png"
#define GS_PayNormal "fukuan.png"
#define GS_PayPressed "fukuanPressed.png"
#define GS_MultiplyFlag "X.png"
#define GS_MoneyFlag "qian.png"
#define GS_FangDajing "fangdajing.png"
#define GS_Sum "=.png"

#define GS_RestartNormal "restartBtn.png"
#define GS_RestartPressed "restartBtnPressed.png"

#define PayTag 3001

using namespace cocos2d;

CCScene *GameShopScene::sceneWithType(kBackMenuType type)
{
    CCScene *scene = CCScene::create();
    GameShopScene *layer = new GameShopScene();
    if (layer && layer->init(type))
    {
        layer->autorelease();
        scene->addChild(layer);
    }
    else
    {
        CC_SAFE_DELETE(layer);
    }

    return scene;
}

bool GameShopScene::init(kBackMenuType type)
{
    if (!CCLayer::init()) {
        return false;
    }
    
    backType = type;
    
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    CCSprite *bgSprite = CCSprite::create(GS_BG);
    bgSprite->setPosition(CCPointMake(winSize.width/2, winSize.height/2));
    this->addChild(bgSprite);
    
    CCMenu *fangMenu = CCMenu::create(NULL);
    
    CCSprite *fangdajingSprite = CCSprite::create(GS_FangDajing);
    CCMenuItemSprite *fangItem = CCMenuItemSprite::create(fangdajingSprite, NULL);
    fangMenu->addChild(fangItem);
    
    fangdajingSprite = CCSprite::create(GS_FangDajing);
    fangItem = CCMenuItemSprite::create(fangdajingSprite, NULL);
    fangMenu->addChild(fangItem);
    
    fangdajingSprite = CCSprite::create(GS_FangDajing);
    fangItem = CCMenuItemSprite::create(fangdajingSprite, NULL);
    fangMenu->addChild(fangItem);
       
    fangMenu->alignItemsVerticallyWithPadding(fangdajingSprite->getContentSize().height*0.2);
    
    fangMenu->setPosition(ccp(winSize.width/4, winSize.height*0.48));
    this->addChild(fangMenu);
    
    CCMenu *multiplyMenu = CCMenu::create(NULL);
    CCSprite *multiplySprite = NULL;
    CCMenuItemSprite *multiplyItem = NULL;
    multiplySprite = CCSprite::create(GS_MultiplyFlag);
    multiplyItem = CCMenuItemSprite::create(multiplySprite, NULL);
    multiplyMenu->addChild(multiplyItem);
    multiplySprite = CCSprite::create(GS_MultiplyFlag);
    multiplyItem = CCMenuItemSprite::create(multiplySprite, NULL);
    multiplyMenu->addChild(multiplyItem);
    multiplySprite = CCSprite::create(GS_MultiplyFlag);
    multiplyItem = CCMenuItemSprite::create(multiplySprite, NULL);
    multiplyMenu->addChild(multiplyItem);
    multiplyMenu->alignItemsVerticallyWithPadding(multiplySprite->getContentSize().height*1.8);
    multiplyMenu->setPosition(ccp(winSize.width*0.35, winSize.height*0.47));
    this->addChild(multiplyMenu);
    
    CCMenu *totalNumberMenu = CCMenu::create(NULL);
    CCSprite *totalNumber = CCSprite::create();
    CCSprite *firstN = CCSprite::create("1.png");
    firstN->setPosition(ccp(firstN->getContentSize().width/2, firstN->getContentSize().height/2));
    totalNumber->addChild(firstN);
    firstN = CCSprite::create("2.png");
    firstN->setPosition(ccp(firstN->getContentSize().width, firstN->getContentSize().height/2));
    totalNumber->addChild(firstN);
    firstN = CCSprite::create("0.png");
    firstN->setPosition(ccp(firstN->getContentSize().width*2, firstN->getContentSize().height/2));
    totalNumber->addChild(firstN);
    CCMenuItemSprite *totalNumberItem = CCMenuItemSprite::create(totalNumber, NULL);
    totalNumberMenu->addChild(totalNumberItem);
    
    totalNumber = CCSprite::create();
    firstN = CCSprite::create("5.png");
    firstN->setPosition(ccp(firstN->getContentSize().width/2, firstN->getContentSize().height/2));
    totalNumber->addChild(firstN);
    firstN = CCSprite::create("0.png");
    firstN->setPosition(ccp(firstN->getContentSize().width*1.5, firstN->getContentSize().height/2));
    totalNumber->addChild(firstN);
    totalNumberItem = CCMenuItemSprite::create(totalNumber, NULL);
    totalNumberMenu->addChild(totalNumberItem);

    totalNumber = CCSprite::create();
    firstN = CCSprite::create("2.png");
    firstN->setPosition(ccp(firstN->getContentSize().width/2, firstN->getContentSize().height/2));
    totalNumber->addChild(firstN);
    firstN = CCSprite::create("0.png");
    firstN->setPosition(ccp(firstN->getContentSize().width*1.5, firstN->getContentSize().height/2));
    totalNumber->addChild(firstN);
    totalNumberItem = CCMenuItemSprite::create(totalNumber, NULL);
    totalNumberMenu->addChild(totalNumberItem);

    totalNumberMenu->setPosition(ccp(winSize.width*0.4, winSize.height*0.445));
    totalNumberMenu->alignItemsVerticallyWithPadding(firstN->getContentSize().height*3.5);
    this->addChild(totalNumberMenu);
    
    CCMenu *sumMenu = CCMenu::create(NULL);
    CCSprite *sumSprite = CCSprite::create(GS_Sum);
    CCMenuItemSprite *sumItem = CCMenuItemSprite::create(sumSprite, NULL);
    sumMenu->addChild(sumItem);
    sumSprite = CCSprite::create(GS_Sum);
    sumItem = CCMenuItemSprite::create(sumSprite, NULL);
    sumMenu->addChild(sumItem);
    sumSprite = CCSprite::create(GS_Sum);
    sumItem = CCMenuItemSprite::create(sumSprite, NULL);
    sumMenu->addChild(sumItem);
    
    sumMenu->setPosition(ccp(winSize.width*0.5, winSize.height*0.47));
    sumMenu->alignItemsVerticallyWithPadding(sumSprite->getContentSize().height*5);
    this->addChild(sumMenu);

    
    CCMenu *flagMenu = CCMenu::create(NULL);
    
    CCSprite *flagSprite = CCSprite::create(GS_MoneyFlag);
    CCMenuItemSprite *flagItem = CCMenuItemSprite::create(flagSprite, NULL);
    flagMenu->addChild(flagItem);
    
    flagSprite = CCSprite::create(GS_MoneyFlag);
    flagItem = CCMenuItemSprite::create(flagSprite, NULL);
    flagMenu->addChild(flagItem);

    flagSprite = CCSprite::create(GS_MoneyFlag);
    flagItem = CCMenuItemSprite::create(flagSprite, NULL);
    flagMenu->addChild(flagItem);

    flagMenu->setPosition(CCPointMake(winSize.width *0.55, winSize.height*0.465));
    flagMenu->alignItemsVerticallyWithPadding(flagSprite->getContentSize().height*1.7);
    this->addChild(flagMenu);
    
    CCMenu *moneyMenu = CCMenu::create(NULL);
    CCSprite *moneySprite = CCSprite::create();
    firstN = CCSprite::create("1.png");
    firstN->setPosition(ccp(firstN->getContentSize().width/2, firstN->getContentSize().height/2));
    moneySprite->addChild(firstN);
    firstN = CCSprite::create("8.png");
    firstN->setPosition(ccp(firstN->getContentSize().width*1.3, firstN->getContentSize().height/2));
    moneySprite->addChild(firstN);

    CCMenuItemSprite *moneyItem = CCMenuItemSprite::create(moneySprite, NULL);
    moneyMenu->addChild(moneyItem);
    
    moneySprite = CCSprite::create();
    firstN = CCSprite::create("1.png");
    firstN->setPosition(ccp(firstN->getContentSize().width/2, firstN->getContentSize().height/2));
    moneySprite->addChild(firstN);
    firstN = CCSprite::create("2.png");
    firstN->setPosition(ccp(firstN->getContentSize().width*1.3, firstN->getContentSize().height/2));
    moneySprite->addChild(firstN);
    
    moneyItem = CCMenuItemSprite::create(moneySprite, NULL);
    moneyMenu->addChild(moneyItem);
    
    moneySprite = CCSprite::create();
    firstN = CCSprite::create("6.png");
    firstN->setPosition(ccp(firstN->getContentSize().width/2, firstN->getContentSize().height/2));
    moneySprite->addChild(firstN);
    
    moneyItem = CCMenuItemSprite::create(moneySprite, NULL);
    moneyMenu->addChild(moneyItem);
    
    moneyMenu->setPosition(CCPointMake(winSize.width*0.58, winSize.height*0.445));
    moneyMenu->alignItemsVerticallyWithPadding(firstN->getContentSize().height*3.5);
    this->addChild(moneyMenu);
    
    
    CCMenu *payMenu = CCMenu::create(NULL);

    CCSprite *payforNormal = CCSprite::create(GS_PayNormal);
    CCSprite *payforSelected = CCSprite::create(GS_PayPressed);
    
    CCMenuItemSprite *payforItem = CCMenuItemSprite::create(payforNormal, payforSelected, this, menu_selector(GameShopScene::payfor));
    payforItem->setTag(PayTag + 1);
    payMenu->addChild(payforItem);
    
    payforNormal = CCSprite::create(GS_PayNormal);
    payforSelected = CCSprite::create(GS_PayPressed);
    
    payforItem = CCMenuItemSprite::create(payforNormal, payforSelected, this, menu_selector(GameShopScene::payfor));
    payforItem->setTag(PayTag + 2);
    payMenu->addChild(payforItem);

    payforNormal = CCSprite::create(GS_PayNormal);
    payforSelected = CCSprite::create(GS_PayPressed);
    
    payforItem = CCMenuItemSprite::create(payforNormal, payforSelected, this, menu_selector(GameShopScene::payfor));
    payforItem->setTag(PayTag + 3);
    payMenu->addChild(payforItem);
    
    payMenu->setPosition(ccp(winSize.width*0.75, winSize.height*0.47));
    payMenu->alignItemsVerticallyWithPadding(payforNormal->getContentSize().height*0.5);
    this->addChild(payMenu);

    CCSprite *backSprite = CCSprite::create(GS_BackNormal);
    CCSprite *selectedSprite = CCSprite::create(GS_BackPressed);
    CCMenuItemSprite *backItem = CCMenuItemSprite::create(backSprite, selectedSprite, this, menu_selector(GameShopScene::back));
    CCSize backC = backSprite->getContentSize();
    backItem->setPosition(CCPointMake(winSize.width - backC.width, winSize.height - backC.height*1.5));
    CCMenu *backMenu = CCMenu::create(backItem,NULL);
    backMenu->setPosition(CCPointZero);
    this->addChild(backMenu);
    
    return true;
}

void GameShopScene::payfor(cocos2d::CCNode *pSender)
{
    if (NULL == pSender) {
        return;
    }
    int tag = pSender->getTag();
    switch (tag) {
        case PayTag + 1:
        {
            
        }
            break;
        case PayTag + 2:
        {
            
        }
            break;
        case PayTag + 3:
        {
            
        }
            break;
        default:
            break;
    }
}

void GameShopScene::back(cocos2d::CCNode *pSender)
{
    switch (backType) {
        case kBackMainMenuType:
        {
            CCDirector::sharedDirector()->replaceScene(HelloWorld::scene());
        }
            break;
        case kBackPlayMenuType:
        {
            CCDirector::sharedDirector()->popScene();
        }
            break;
        case kBackResultMenuType:
        {
            CCDirector::sharedDirector()->replaceScene(GameResultScene::scene(resultType));
        }
            break;
        default:
            break;
    }
}






