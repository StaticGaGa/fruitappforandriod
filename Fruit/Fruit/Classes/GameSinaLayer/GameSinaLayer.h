//
//  GameSinaLayer.h
//  Fruit
//
//  Created by Static Ga on 13-5-10.
//
//

#ifndef __Fruit__GameSinaLayer__
#define __Fruit__GameSinaLayer__

#include <iostream>
#include "cocos2d.h"

class GameSinaLayer: public cocos2d::CCLayer {
private:
    
public:
    static GameSinaLayer *create();
    bool init();
    virtual void registerWithTouchDispatcher(void);
    bool ccTouchBegan(cocos2d::CCTouch*pTouch, cocos2d::CCEvent *pEvent);
public:
    void itemClicked(CCNode *pSender);
};
#endif /* defined(__Fruit__GameSinaLayer__) */
