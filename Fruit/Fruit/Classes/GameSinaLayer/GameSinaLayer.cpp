//
//  GameSinaLayer.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-10.
//
//

#include "GameSinaLayer.h"
#include "HelloWorldScene.h"

#define kTag_Sina_TextFieldContainer 5002
#define kTag_Sina_TextField 5001

#define kTag_Sina_Share 5003
#define kTag_Sina_Back 5004
using namespace cocos2d;

GameSinaLayer *GameSinaLayer::create()
{
    GameSinaLayer *layer = new GameSinaLayer();
    if (layer && layer->init()) {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return NULL;
}

bool GameSinaLayer::init()
{
    if (!CCLayer::init())
    {
        return false;
    }
    setTouchEnabled(true);
    
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    CCSprite *bg = CCSprite::create("commonBG.png");
    bg->setPosition(CCPointMake(winSize.width/2, winSize.height/2));
    bg->setTag(kTag_Sina_TextFieldContainer);
    this->addChild(bg);
    
    CCTextFieldTTF * textfield  = CCTextFieldTTF::textFieldWithPlaceHolder("ss", CCSizeMake(bg->getContentSize().width*8/10, getContentSize().height*4/5), kCCTextAlignmentLeft, "Thonburi", 20);
    textfield->setString("我刚刚玩了《弹叮铛水果盘》，我觉得你的智力可能过不了几关，不信你来试试");
    textfield->setPosition(ccp(bg->getContentSize().width *0.5, bg->getContentSize().height*0.3));
    textfield->setColor(ccBLACK);
    textfield->setTag(kTag_Sina_TextField);
    bg->addChild(textfield);
    
    CCSprite *shareNormal = CCSprite::create("sharebutton.png");
    CCMenuItemSprite *shareItem = CCMenuItemSprite::create(shareNormal, NULL, this, menu_selector(GameSinaLayer::itemClicked));
    shareItem->setPosition(winSize.width/2, shareNormal->getContentSize().height);
    shareItem->setTag(kTag_Sina_Share);
    CCSprite *backNormal = CCSprite::create("fanhui.png");
    CCMenuItemSprite *backItem = CCMenuItemSprite::create(backNormal, NULL, this, menu_selector(GameSinaLayer::itemClicked));
    backItem->setPosition(winSize.width - backNormal->getContentSize().width,backNormal->getContentSize().height);
    backItem->setTag(kTag_Sina_Back);
    CCMenu *menu = CCMenu::create(shareItem, backItem,NULL);
    menu->setPosition(CCPointZero);
    this->addChild(menu);
    
    return true;
}

void GameSinaLayer::itemClicked(cocos2d::CCNode *pSender)
{
    if (NULL == pSender) {
        return;
    }
    
    int tag = pSender->getTag();
    switch (tag) {
        case kTag_Sina_Share:
        {
            
        }
            break;
        case kTag_Sina_Back:
        {
//            CCDirector::sharedDirector()->replaceScene(HelloWorld::scene());
            this->removeFromParentAndCleanup(true);
        }
            break;
        default:
            break;
    }
}

void GameSinaLayer::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, kCCMenuHandlerPriority, true);
}

bool GameSinaLayer::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent) {
    CCNode *textFieldContainer = this->getChildByTag(kTag_Sina_TextFieldContainer);
    if (textFieldContainer) {
        CCTextFieldTTF *textField = (CCTextFieldTTF *)textFieldContainer->getChildByTag(kTag_Sina_TextField);
        textField->attachWithIME();
        
    }
    return true;
}