//
//  CCPlistData.h
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#ifndef __Fruit__CCPlistData__
#define __Fruit__CCPlistData__

#include <iostream>
class CCPlistData {
public:
    static bool getFirstDJ();
    static void saveFirstDJ();
    static void saveDJNum(int djNum);
    static int getDJNum();
    static void turnOnSound();
    static void turnOffSound();
    static bool getSoundSettings();
    static void setLoginDate();
    static bool shouldShowLoginAwards();
};
#endif /* defined(__Fruit__CCPlistData__) */
