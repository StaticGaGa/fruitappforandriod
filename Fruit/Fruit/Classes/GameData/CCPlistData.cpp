//
//  CCPlistData.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#include "CCPlistData.h"
#include "cocos2d.h"
#include <time.h>

#define DJ_First_Key "DJ_First_Key"
#define DJ_Key "DJ_Key"

#define kSoundValue "soundSetting"
#define kLoginDate "loginDate"


using namespace cocos2d;

bool CCPlistData::getFirstDJ() {
    return CCUserDefault::sharedUserDefault()->getBoolForKey(DJ_First_Key);
}

void CCPlistData::saveFirstDJ() {
    CCUserDefault::sharedUserDefault()->setBoolForKey(DJ_First_Key, true);
    CCPlistData::saveDJNum(3);
}

void CCPlistData::saveDJNum(int djNum) {
    CCUserDefault::sharedUserDefault()->setIntegerForKey(DJ_Key, djNum);
}

int CCPlistData::getDJNum() {
    return CCUserDefault::sharedUserDefault()->getIntegerForKey(DJ_Key);
}

void CCPlistData::turnOnSound() {
    CCUserDefault::sharedUserDefault()->setBoolForKey(kSoundValue, true);
}

void CCPlistData::turnOffSound() {
    CCUserDefault::sharedUserDefault()->setBoolForKey(kSoundValue, false);
}

bool CCPlistData::getSoundSettings() {
    return CCUserDefault::sharedUserDefault()->getBoolForKey(kSoundValue);
}

void CCPlistData::setLoginDate() {
    cc_timeval *startTime=new cc_timeval;
    CCTime::gettimeofdayCocos2d(startTime,NULL);
    double rawTime = (startTime->tv_sec*1000.0+startTime->tv_usec/1000.0);
//    time_t rawtime;
//    time(&rawtime);
//    CCLog("currentTime %lf",(double)rawtime);
    CCUserDefault::sharedUserDefault()->setDoubleForKey(kLoginDate, (rawTime));
    delete startTime;

}

bool CCPlistData::shouldShowLoginAwards() {
    bool awards = true;
    double loginDate = CCUserDefault::sharedUserDefault()->getDoubleForKey(kLoginDate);
    printf("loginDate %f",loginDate);
    if (!loginDate) {
        CCPlistData::setLoginDate();
        return awards;
    }
    cc_timeval *endTime=new cc_timeval;
    CCTime::gettimeofdayCocos2d(endTime,NULL);
    double duration=(endTime->tv_sec*1000.0+endTime->tv_usec/1000.0) - loginDate;
    printf("duration=%f\n",duration/1000);
    delete endTime;
    if (duration/1000 > 24*60*60) {
        CCPlistData::setLoginDate();
        return awards;
    }else {
        awards = false;
        return awards;
    }
}

