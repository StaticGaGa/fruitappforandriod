//
//  CCFruitsData.h
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#ifndef __Fruit__CCFruitsData__
#define __Fruit__CCFruitsData__

#include <iostream>
#include "cocos2d.h"
#include "FruitSprite.h"

class CCFruitsData {
private:
    cocos2d::CCArray *fruitsArray;
    int index;
    int chanceNum;
public:
    CCFruitsData();
    static CCFruitsData *sharedFruits();
    void clearFruits();
    cocos2d::CCArray *getFruits() {return fruitsArray;}
    void indexAdd();
    int getIndex(){return index;}
    void chanceNumSub();
    int getChanceNum(){return chanceNum;}
    void fruitsRange();
    bool fruitsCompare(FruitSprite *sprite);
    cocos2d::CCArray *getFruitsArray();
};
#endif /* defined(__Fruit__CCFruitsData__) */
