//
//  CCFruitsData.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#include "CCFruitsData.h"
using namespace std;
using namespace cocos2d;
#define Fruit_Name "Fruit"
#define Fruit_Num 15
#define First_Fruits_Num 1
#define Chance_Num 3


using namespace cocos2d;

static CCFruitsData *fruits = NULL;

CCFruitsData *CCFruitsData::sharedFruits()
{
    if (NULL == fruits) {
        fruits = new CCFruitsData();
    }
    return fruits;
}

CCFruitsData::CCFruitsData() {
    fruitsArray = new CCArray();
    index = First_Fruits_Num;
    chanceNum = Chance_Num;
}

void CCFruitsData::clearFruits()
{
//    CC_SAFE_RELEASE(fruitsArray);
    fruitsArray->removeAllObjects();
}

void CCFruitsData::indexAdd() {
    index++;
}

void CCFruitsData::chanceNumSub() {
    if (chanceNum <= 0) {
        chanceNum = 0;
        return;
    }
    chanceNum--;
}

void CCFruitsData::fruitsRange() {
    if (fruitsArray && fruitsArray->count() ) {
        fruitsArray->removeAllObjects();
    }

    CCArray *tmpArray = this->getFruitsArray();
    for (int i = 0; i < index; i++) {
        int randNum = arc4random()%tmpArray->count();
        FruitSprite *object = (FruitSprite *)tmpArray->objectAtIndex(randNum);
        object->setFramePoint(i);
        fruitsArray->addObject(object);
        tmpArray->removeObject(object);
    }
}

bool CCFruitsData::fruitsCompare(FruitSprite *sprite)
{
    bool isTrue = true;
    int count = fruitsArray->count();
    FruitSprite *findSprite = NULL;
    for (int num = 0; num < count; num++) {
        FruitSprite *tmpSprite = (FruitSprite *)fruitsArray->objectAtIndex(num);
        if (tmpSprite->getFramePoint() == sprite->getFramePoint()) {
            findSprite = tmpSprite;
            break;
        }
    }
    
    if (findSprite) {
        if (findSprite->getTexture() != sprite->getTexture()) {
            isTrue = false;
        }
    }else {
        isTrue = false;
    }
    
    return isTrue;
}

CCArray *CCFruitsData::getFruitsArray() {
    CCArray *tmpArray = new CCArray();
    for (int i = 0; i < Fruit_Num;i++) {
        CCString *tmpStr = CCString::createWithFormat("Fruit%.2d.png",i + 1);
        FruitSprite *sprite = FruitSprite::create(tmpStr->getCString());
        sprite->setFramePoint(i);
        tmpArray->addObject(sprite);
    }
    tmpArray->autorelease();
    return tmpArray;
}

