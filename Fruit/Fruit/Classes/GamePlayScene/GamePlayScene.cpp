//
//  GamePlayScene.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#include "GamePlayScene.h"
#include "CCPlistData.h"
#include "HelpNumSprite.h"
#include "CCFruitsData.h"
#include "GameShopScene.h"
#include "GameCompareScene.h"

using namespace cocos2d;

#define Background "Memory_Background.png"
#define Left_Frame "caozuolan.png"
#define Right_Frame "bingxiang.png"

#define Help_Menu "fangdajing.png"
#define Help_Menu_Selected "fangdajing.png"
#define Finish_Menu "querenanniu.png"
#define Finish_Menu_Selected "querenanniu.png"
#define Pause_Menu "zantinganniu.png"
#define Pause_Menu_Selected "zantinganniu.png"

#define Restart_Menu "restartBtn.png"
#define Restart_Menu_Selected "restartBtnPressed.png"

#define Left_Frame_Tag 1
#define Right_Frame_Tag 2
#define Help_Num_Tag 3
#define Move_Time 0.75

#define Fruit_Num 15
#define Fruit_ZOrder 10

#define Change_Time 0.25

#define kPauseTag 7001
#define kHelpTag 7002
#define kFinishedTag 7003

CCScene *GamePlayScene::scene() {
    CCScene *scene = CCScene::create();
    GamePlayScene *layer = GamePlayScene::create();
    scene->addChild(layer);
    return scene;
}

bool GamePlayScene::init() {
    if (!CCLayer::init()) {
        return false;
    }
    this->setTouchEnabled(true);
    showArray = new CCArray();
    putArray = new CCArray();
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    CCSprite *bg = CCSprite::create(Background);
    bg->setPosition(ccp(winSize.width/2, winSize.height/2));
    this->addChild(bg);
    
    CCSprite *rightSprite = CCSprite::create(Right_Frame);
    CCSize cttSize = rightSprite->getContentSize();
    rightSprite->setTag(Right_Frame_Tag);
    rightSprite->setPosition(ccp(winSize.width + cttSize.width/2, cttSize.height/2));
    this->addChild(rightSprite);
    
    CCMoveBy *moveAct = CCMoveBy::create(Move_Time, ccp(-cttSize.width, 0));
    rightSprite->runAction(moveAct);
    
    CCSprite *leftSprite = CCSprite::create(Left_Frame);
    cttSize = leftSprite->getContentSize();
    leftSprite->setTag(Left_Frame_Tag);
    leftSprite->setPosition(ccp(-cttSize.width/2, cttSize.height/2));
    this->addChild(leftSprite);
        
    CCSprite *normalSprite = CCSprite::create(Pause_Menu);
    CCSprite *selectedSprite = CCSprite::create(Pause_Menu_Selected);
    
    CCMenuItemSprite *pauseItem = CCMenuItemSprite::create(normalSprite, selectedSprite, this, menu_selector(GamePlayScene::itemClicked));
    pauseItem->setTag(kPauseTag);
    pauseItem->setPosition(ccp(cttSize.width/5, cttSize.height/4*3));
    
    normalSprite = CCSprite::create(Help_Menu);
    selectedSprite->create(Help_Menu_Selected);
    CCMenuItemSprite *helpItem = CCMenuItemSprite::create(normalSprite, NULL, this, menu_selector(GamePlayScene::itemClicked));
    helpItem->setTag(kHelpTag);
    helpItem->setPosition(ccp(cttSize.width/5, cttSize.height/2));
    
    normalSprite = CCSprite::create(Finish_Menu);
    selectedSprite = CCSprite::create(Finish_Menu_Selected);
    CCMenuItemSprite *finishedItem = CCMenuItemSprite::create(normalSprite, selectedSprite, this, menu_selector(GamePlayScene::itemClicked));
    finishedItem->setTag(kFinishedTag);
    finishedItem->setPosition(ccp(cttSize.width/5, cttSize.height/4));
    
    CCMenu *menu = CCMenu::create(pauseItem,helpItem,finishedItem,NULL);
    menu->setPosition(CCPointZero);
    leftSprite->addChild(menu);
    
    HelpNumSprite *helpNumSprite = HelpNumSprite::create();
    cttSize = helpItem->getContentSize();
    helpNumSprite->setPosition(CCPointMake(cttSize.width, cttSize.height));
    helpNumSprite->setTag(Help_Num_Tag);
    helpItem->addChild(helpNumSprite);
    
    cttSize = leftSprite->getContentSize();
    CCArray *array = CCFruitsData::sharedFruits()->getFruitsArray();
    for (int num = 0; num < Fruit_Num; num++) {
        FruitSprite *sprite = (FruitSprite *)array->objectAtIndex(num);
        sprite->setFramePoint(num + Fruit_Num);
        sprite->setPosition(this->getPointFromFramePoint(sprite->getFramePoint()));
        leftSprite->addChild(sprite, Fruit_ZOrder);
        
        CCMoveBy *moveAt = CCMoveBy::create(Move_Time, ccp(cttSize.width,0));
        sprite->runAction(moveAt);
        showArray->addObject(sprite);
    }
    
    moveAct = CCMoveBy::create(Move_Time, ccp(cttSize.width, 0));
    leftSprite->runAction(moveAct);

    return true;
}
#if 0
void GamePlayScene::registerWithTouchDispatcher() {
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 0, true);
}
#endif

void GamePlayScene::itemClicked(cocos2d::CCObject *pObj)
{
    if (pObj) {
        CCNode *pNode = (CCNode *)pObj;
        int tag = pNode->getTag();
        switch (tag) {
            case kPauseTag:
            {
                CCDirector::sharedDirector()->pushScene(GameShopScene::sceneWithType(GameShopScene::kBackPlayMenuType));
            }
                break;
            case kHelpTag:
            {
                this->helpClicked((CCNode *)pObj);
            }
                break;
            case kFinishedTag:
            {
                this->finishMenu();
            }
                break;
            default:
                break;
        }
    }
}

void GamePlayScene::helpClicked(CCNode *node) {
    int helpNum = CCPlistData::getDJNum();
    if (helpNum < 1) {
        return;
    }
    
    CCArray *array = CCFruitsData::sharedFruits()->getFruits();
    int count = array->count();
    int tmpCount = putArray->count();
    for (int num = 0; num < count; num++) {
        FruitSprite *suredSprite = (FruitSprite *)array->objectAtIndex(num);
        FruitSprite *changeSprite = NULL;
        bool canChange = true;
        for (int tmpNum = 0; tmpNum < tmpCount; tmpNum++) {
            FruitSprite *sprite = (FruitSprite *)putArray->objectAtIndex(tmpNum);
            if (suredSprite->getFramePoint() == sprite->getFramePoint()) {
                if (sprite->getTexture() != suredSprite->getTexture()) {
                    changeSprite = sprite;
                    break;
                }else {
                    canChange = false;
                }
            }
        }
        if (canChange) {
            CCPlistData::saveDJNum(--helpNum);
            node->removeChildByTag(Help_Num_Tag, true);
            
            CCSize cttSize = node->getContentSize();
            HelpNumSprite *helpSprite = HelpNumSprite::create();
            helpSprite->setPosition(ccp(cttSize.width, cttSize.height));
            helpSprite->setTag(Help_Num_Tag);
            node->addChild(helpSprite);

            this->setSprite(this->findFruitSprite(suredSprite), num, true);
            break;
        }
    }
}

void GamePlayScene::finishMenu() {
    this->fruitsMoveAway();
    CCDelayTime *delayAct = CCDelayTime::create(Move_Time);
    CCCallFunc *callAct = CCCallFunc::create(this, callfunc_selector(GamePlayScene::goNext));
    this->runAction(CCSequence::create(delayAct,callAct,NULL));
}

void GamePlayScene::goNext() {
    CCDirector::sharedDirector()->replaceScene(GameCompareScene::scene(putArray));
}

void GamePlayScene::fruitsMoveAway() {
    CCSprite *leftFrame = (CCSprite *)this->getChildByTag(Left_Frame_Tag);
    CCSize cttSize = leftFrame->getContentSize();
    CCMoveBy *moveAct = CCMoveBy::create(Move_Time, ccp(- cttSize.width, 0));
    leftFrame->runAction(moveAct);
    
    int count = showArray->count();
    for (int num = 0; num < count;num++) {
        FruitSprite *sprite = (FruitSprite *)showArray->objectAtIndex(num);
        moveAct = CCMoveBy::create(Move_Time, ccp(-cttSize.width, 0));
        sprite->runAction(moveAct);
    }
    
    CCSprite *rightFrame = (CCSprite *)this->getChildByTag(Right_Frame_Tag);
    cttSize = rightFrame->getContentSize();
    moveAct = CCMoveBy::create(Move_Time, ccp(cttSize.width, 0));
    rightFrame->runAction(moveAct);
    
    count = putArray->count();
    for (int num = 0; num < count;num++) {
        FruitSprite *sprite = (FruitSprite *)putArray->objectAtIndex(num);
        moveAct = CCMoveBy::create(Move_Time, CCPointMake(3*cttSize.width, 0));
        sprite->runAction(moveAct);
    }
}

FruitSprite *GamePlayScene::findFruitSprite(FruitSprite *sprite) {
    FruitSprite *fruitSprite = NULL;
    int count = showArray->count();
    for (int num = 0; num < count;num++) {
        FruitSprite *tmpSprite = (FruitSprite *)showArray->objectAtIndex(num);
        if (sprite->getTexture() == tmpSprite->getTexture()) {
            fruitSprite = tmpSprite;
            break;
        }
    }
    
    count = putArray->count();
    for (int num = 0; num < count; num++) {
        FruitSprite *tmpSprite = (FruitSprite *)putArray->objectAtIndex(num);
        if (sprite->getTexture() == tmpSprite->getTexture()) {
            fruitSprite = tmpSprite;
            break;
        }
    }
    return fruitSprite;
}

CCPoint GamePlayScene::getPointFromFramePoint(int framePoint) {
    if (framePoint < Fruit_Num) {
        return this->getRightPointFromFramePoint(framePoint);
    }else {
        return this->getLeftPointFromFramePoint(framePoint%Fruit_Num);
    }
}

CCPoint GamePlayScene::getRightPointFromFramePoint(int framePoint) {
    CCSprite *frameSprite = (CCSprite *)this->getChildByTag(Right_Frame_Tag);
    CCSize cttSize = frameSprite->getContentSize();
    CCPoint point;
    switch(framePoint)
    {
        case 0:
        {
            point = CCPointMake(cttSize.width/4, cttSize.height/80*15);
            break;
        }
        case 1:
        {
            point = CCPointMake(cttSize.width/2, cttSize.height/80*15);
            break;
        }
        case 2:
        {
            point = CCPointMake(cttSize.width/4*3, cttSize.height/80*15);
            break;
        }
        case 3:
        {
            point = CCPointMake(cttSize.width/4, cttSize.height/80*28);
            break;
        }
        case 4:
        {
            point = CCPointMake(cttSize.width/2, cttSize.height/80*28);
            break;
        }
        case 5:
        {
            point = CCPointMake(cttSize.width/4*3, cttSize.height/80*28);
            break;
        }
        case 6:
        {
            point = CCPointMake(cttSize.width/4, cttSize.height/80*41);
            break;
        }
        case 7:
        {
            point = CCPointMake(cttSize.width/2, cttSize.height/80*41);
            break;
        }
        case 8:
        {
            point = CCPointMake(cttSize.width/4*3, cttSize.height/80*41);
            break;
        }
        case 9:
        {
            point = CCPointMake(cttSize.width/4, cttSize.height/80*54);
            break;
        }
        case 10:
        {
            point = CCPointMake(cttSize.width/2, cttSize.height/80*54);
            break;
        }
        case 11:
        {
            point = CCPointMake(cttSize.width/4*3, cttSize.height/80*54);
            break;
        }
        case 12:
        {
            point = CCPointMake(cttSize.width/4, cttSize.height/80*67);
            break;
        }
        case 13:
        {
            point = CCPointMake(cttSize.width/2, cttSize.height/80*67);
            break;
        }
        case 14:
        {
            point = CCPointMake(cttSize.width/4*3, cttSize.height/80*67);
            break;
        }
    }
    CCPoint position = frameSprite->getPosition();
    point.x = position.x - cttSize.width/2 + point.x;
    point.y = position.y - cttSize.height/2 + point.y;
    return point;
}

CCPoint GamePlayScene::getLeftPointFromFramePoint(int framePoint) {
    CCSprite *frameSprite = (CCSprite*)this->getChildByTag(Left_Frame_Tag);
    CCSize cttSize = frameSprite->getContentSize();
    
    CCPoint point;
    switch(framePoint)
    {
        case 0:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*14);
            break;
        }
        case 1:
        {
            point = CCPointMake(cttSize.width/20*12, cttSize.height/80*14);
            break;
        }
        case 2:
        {
            point = CCPointMake(cttSize.width/20*15, cttSize.height/80*14);
            break;
        }
        case 3:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*27);
            break;
        }
        case 4:
        {
            point = CCPointMake(cttSize.width/20*12, cttSize.height/80*27);
            break;
        }
        case 5:
        {
            point = CCPointMake(cttSize.width/20*15, cttSize.height/80*27);
            break;
        }
        case 6:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*40);
            break;
        }
        case 7:
        {
            point = CCPointMake(cttSize.width/20*12, cttSize.height/80*40);
            break;
        }
        case 8:
        {
            point = CCPointMake(cttSize.width/20*15, cttSize.height/80*40);
            break;
        }
        case 9:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*52);
            break;
        }
        case 10:
        {
            point = CCPointMake(cttSize.width/20*12, cttSize.height/80*52);
            break;
        }
        case 11:
        {
            point = CCPointMake(cttSize.width/20*15, cttSize.height/80*52);
            break;
        }
        case 12:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*65);
            break;
        }
        case 13:
        {
            point = CCPointMake(cttSize.width/20*12, cttSize.height/80*65);
            break;
        }
        case 14:
        {
            point = CCPointMake(cttSize.width/20*15, cttSize.height/80*65);
            break;
        }
    }
    CCPoint position = frameSprite->getPosition();
    point.x = position.x - cttSize.width/2 + point.x;
    point.y = position.y - cttSize.height/2 + point.y;
    return point;
}

void GamePlayScene::ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent) {
    CCLog("touch");
    CCTouch *pTouch = (CCTouch *)pTouches->anyObject();
    CCPoint location = pTouch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    int count = showArray->count();
    for (int num = 0; num < count; num++) {
        FruitSprite *sprite = (FruitSprite *)showArray->objectAtIndex(num);
        CCRect rect;
        rect.origin = CCPointZero;
        rect.size = sprite->getContentSize();
        CCPoint point = sprite->convertToNodeSpace(location);
        if (rect.containsPoint(point)) {
            moveSprite = sprite;
            beginPoint = location;
            return;
        }
    }
    
    count = putArray->count();
    for (int num = 0; num < count; num++) {
        FruitSprite *sprite = (FruitSprite *)putArray->objectAtIndex(num);
        CCRect rect;
        rect.origin = CCPointZero;
        rect.size = sprite->getContentSize();
        CCPoint point = sprite->convertToNodeSpace(location);
        if (rect.containsPoint(point)) {
            moveSprite = sprite;
            beginPoint = location;
            return;
        }
    }
    
    return;
}

void GamePlayScene::ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent) {
    if (moveSprite) {
        CCTouch *pTouch = (CCTouch *)pTouches->anyObject();
        CCPoint location = pTouch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        
        CCPoint position = moveSprite->getPosition();
        CCPoint nePos;
        nePos.x = location.x - beginPoint.x + position.x;
        nePos.y = location.y - beginPoint.y + position.y;
        moveSprite->setPosition(nePos);
        beginPoint = location;
    }
}

void GamePlayScene::ccTouchesEnded(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent) {
    if (moveSprite) {
        bool isBack = true;
        CCSize cttSize = moveSprite->getContentSize();
        for (int num = 0; num < Fruit_Num*2; num++) {
            CCPoint location = this->getPointFromFramePoint(num);
            CCRect rect = CCRectMake(location.x - cttSize.width/2, location.y - cttSize.height/2, cttSize.width, cttSize.height);
            if (rect.containsPoint(moveSprite->getPosition())) {
                isBack = false;
                this->setSprite(moveSprite, num, false);
                break;
            }
        }
        
        if (isBack) {
            moveSprite->setPosition(this->getPointFromFramePoint(moveSprite->getFramePoint()));
        }
        moveSprite = NULL;
    }

}


void GamePlayScene::setSprite(FruitSprite *tmpMoveSprite, int tmpFramPoint, bool act) {
    FruitSprite *changeSprite = NULL;
    int count = showArray->count();
    for (int num = 0; num < count; num++) {
        FruitSprite *sprite = (FruitSprite *)showArray->objectAtIndex(num);
        if (sprite->getFramePoint() == tmpFramPoint) {
            changeSprite = sprite;
            break;
        }
    }
    
    count = putArray->count();
    for (int num = 0; num < count;num++) {
        FruitSprite *sprite = (FruitSprite *)putArray->objectAtIndex(num);
        if (sprite->getFramePoint() == tmpFramPoint) {
            changeSprite = sprite;
            break;
        }
    }
    
    if (changeSprite) {
        int tmp = tmpMoveSprite->getFramePoint();
        tmpMoveSprite->setFramePoint(changeSprite->getFramePoint());
        changeSprite->setFramePoint(tmp);
        
        if (act) {
            CCScaleTo *scaleAct = CCScaleTo::create(Change_Time, 0, 1);
            CCScaleTo *rescaleAct = CCScaleTo::create(Change_Time, 1, 1);
            CCCallFuncN *callAct = CCCallFuncN::create(this, callfuncN_selector(GamePlayScene::animationCallBack));
            tmpMoveSprite->runAction(CCSequence::create(scaleAct,callAct,rescaleAct,NULL));
        
            scaleAct = CCScaleTo::create(Change_Time, 0, 1);
            rescaleAct = CCScaleTo::create(Change_Time, 1, 1);
            callAct = CCCallFuncN::create(this, callfuncN_selector(GamePlayScene::animationCallBack));
            changeSprite->runAction(CCSequence::create(scaleAct,callAct,rescaleAct,NULL));
        }else {
            tmpMoveSprite->setPosition(this->getPointFromFramePoint(tmpMoveSprite->getFramePoint()));
            changeSprite->setPosition(this->getPointFromFramePoint(changeSprite->getFramePoint()));
        }
    }else {
        tmpMoveSprite->setFramePoint(tmpFramPoint);
        if (act) {
            CCScaleTo *scaleAct = CCScaleTo::create(Change_Time, 0, 1);
            CCScaleTo *rescaleAct = CCScaleTo::create(Change_Time, 1, 1);
            CCCallFuncN *callAct = CCCallFuncN::create(this, callfuncN_selector(GamePlayScene::animationCallBack));
            tmpMoveSprite->runAction(CCSequence::create(scaleAct,callAct,rescaleAct,NULL));

        }else {
            tmpMoveSprite->setPosition(this->getPointFromFramePoint(tmpMoveSprite->getFramePoint()));
        }
    }
    
    if (showArray->containsObject(tmpMoveSprite)) {
        if (tmpFramPoint < Fruit_Num) {
            putArray->addObject(tmpMoveSprite);
            showArray->removeObject(tmpMoveSprite);
            if (changeSprite) {
                showArray->addObject(changeSprite);
                putArray->removeObject(changeSprite);
            }
        }
    }else {
        if (tmpFramPoint >= Fruit_Num) {
            showArray->addObject(tmpMoveSprite);
            putArray->removeObject(tmpMoveSprite);
            if (changeSprite) {
                putArray->addObject(changeSprite);
                showArray->removeObject(changeSprite);
            }
        }
    }
}

void GamePlayScene::animationCallBack(cocos2d::CCNode *pNode) {
    if (pNode) {
        FruitSprite *sprite = (FruitSprite *)pNode;
        sprite->setPosition(this->getPointFromFramePoint(sprite->getFramePoint()));
    }
}

