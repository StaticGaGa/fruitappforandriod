//
//  GameResultScene.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#include "GameResultScene.h"
#include "CCFruitsData.h"
#include "HelpNumSprite.h"
#include "GameMemoryScene.h"
#include "HelloWorldScene.h"
#include "GameShopScene.h"

#define R_BG "tongyong.png"

#define R_ResumeNormal "chongxintiaozhan.png"
#define R_ResumePressed "chongxintiaozhanPressed.png"

#define R_FangDaJingNormal "fangdajing.png"

#define R_FangDaJingJiaoBiao "fangdajingjiaobiao.png"

#define R_StoreNormal "gouwuchefen.png"
#define R_StorePressed "gouwuchefenPressed.png"

#define R_GoOnNormal "jixu.png"
#define R_GoOnPressed "jixuPressed.png"

#define R_Success "guoguanxingxing.png"
#define R_Failed "guoguanxingxing1.png"

#define R_SuccessDingDang "guoguandandingdang.png"
#define R_FaileDingDang "sibairenwu.png"

#define R_GameFinishedBG "tongyongjiemian03.png"
#define R_GoBack "fanhui.png"

#define kChanceItemTag 7878
#define kContinueItemTag 7879
#define kStoreItemTag 7880

using namespace cocos2d;

CCScene *GameResultScene::scene(int result) {
    CCScene *scene = CCScene::create();
    GameResultScene *layer = new GameResultScene();
    layer->init(result);
    scene->addChild(layer);
    layer->autorelease();
    return scene;
}

bool GameResultScene::init(int theResult) {
    if (!CCLayer::init()) {
        return false;
    }
    result = (kResultType)theResult;
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    CCSprite *bgSprite = NULL;
    if (kResultFinished == result) {
        bgSprite = CCSprite::create(R_GameFinishedBG);
        bgSprite->setPosition(ccp(winSize.width/2, winSize.height/2));
        this->addChild(bgSprite);
        
        CCSprite *normalSprite = CCSprite::create(R_GoBack);
        CCMenuItemSprite *backItem = CCMenuItemSprite::create(normalSprite, NULL, this, menu_selector(GameResultScene::itemClicked));
        backItem->setPosition(ccp(winSize.width - backItem->getContentSize().width*2, backItem->getContentSize().height));
        CCMenu *menu = CCMenu::create(backItem,NULL);
        menu->setPosition(CCPointZero);
        this->addChild(menu);
    }else {
        bgSprite = CCSprite::create(R_BG);
        bgSprite->setPosition(CCPointMake(winSize.width/2, winSize.height/2));
        this->addChild(bgSprite);
        
        this->setUI();
        const char *normalContinueImageName;
        const char *pressedContinueImageName;
        switch (result) {
            case kResultSuccess:
            {
                
            }
                break;
            case kResultFailed:
            {
                normalContinueImageName = R_ResumeNormal;
                pressedContinueImageName = R_ResumePressed;
            }
                break;
            default:
                break;
        }
        
        if (CCFruitsData::sharedFruits()->getChanceNum() <= 0) {
            normalContinueImageName = R_ResumeNormal;
            pressedContinueImageName = R_ResumePressed;
        }else {
            normalContinueImageName = R_GoOnNormal;
            pressedContinueImageName = R_GoOnPressed;
        }
        
        CCSprite *normalSprite = CCSprite::create(R_FangDaJingNormal);
        CCMenuItemSprite *chanceMenu = CCMenuItemSprite::create(normalSprite, NULL, this, menu_selector(GameResultScene::itemClicked));
        chanceMenu->setTag(kChanceItemTag);
        HelpNumSprite *helpNumSprite = HelpNumSprite::create();
        CCSize cttSize = chanceMenu->getContentSize();
        helpNumSprite->setPosition(ccp(cttSize.width, cttSize.height));
        chanceMenu->addChild(helpNumSprite);
        
        chanceMenu->setPosition(ccp(2*chanceMenu->getContentSize().width, chanceMenu->getContentSize().height));
        
        normalSprite = CCSprite::create(normalContinueImageName);
        CCSprite *selectedSprite = CCSprite::create(pressedContinueImageName);
        CCMenuItemSprite *continueMenu = CCMenuItemSprite::create(normalSprite, selectedSprite, this, menu_selector(GameResultScene::itemClicked));
        continueMenu->setTag(kContinueItemTag);
        continueMenu->setPosition(ccp(winSize.width/2, chanceMenu->getContentSize().height));
        normalSprite = CCSprite::create(R_StoreNormal);
        selectedSprite = CCSprite::create(R_StorePressed);
        CCMenuItemSprite *goToStore = CCMenuItemSprite::create(normalSprite, selectedSprite, this, menu_selector(GameResultScene::itemClicked));
        goToStore->setTag(kStoreItemTag);
        goToStore->setPosition(ccp(winSize.width - 2*goToStore->getContentSize().width, goToStore->getContentSize().height));
        CCMenu *menu = CCMenu::create(chanceMenu,continueMenu,goToStore,NULL);
        menu->setPosition(CCPointZero);
        this->addChild(menu);
    }
    return true;
}

void GameResultScene::setUI() {
    CCSprite *sprite = NULL;
    
    if (kResultSuccess == result) {
        sprite = CCSprite::create(R_SuccessDingDang);
    }else {
        sprite = CCSprite::create(R_FaileDingDang);
    }
    
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    sprite->setPosition(ccp(winSize.width - sprite->getContentSize().width*3/2, winSize.height/2));
    this->addChild(sprite);
    
    //暂时标记剩余提示机会（放大镜），这个可能需要从plist里面读取
    int totoalChances = CCFruitsData::sharedFruits()->getChanceNum();

    CCSprite *starSprite = CCSprite::create(R_Success);
    float centerX = winSize.width - sprite->getContentSize().width*2 - starSprite->getContentSize().width/2;
    for (int i = 0; i < totoalChances; i++) {
        starSprite = CCSprite::create(R_Success);
        starSprite->setPosition(ccp(centerX, winSize.height/2));
        this->addChild(starSprite);
        centerX -= starSprite->getContentSize().width;
    }
    for (int i = 0; i < 3 - totoalChances;i++) {
        starSprite = CCSprite::create(R_Failed);
        starSprite->setPosition(ccp(centerX, winSize.height/2));
        centerX -= starSprite->getContentSize().width;
        this->addChild(starSprite);
    }
}

void GameResultScene::itemClicked(cocos2d::CCObject *pObj) {
    if (pObj) {
        CCNode *pNode = (CCNode *)pObj;
        int tag = pNode->getTag();
        switch (tag) {
            case kChanceItemTag:
            {
            
            }
                break;
            case kContinueItemTag:
            {
                this->goOnGame();
            }
                break;
            case kStoreItemTag:
            {
                this->goToStore();
            }
                break;
            default:
                break;
        }
    }
}

void GameResultScene::goOnGame() {
    switch (result) {
        case kResultSuccess:
        {
            CCFruitsData::sharedFruits()->indexAdd();
            if (CCFruitsData::sharedFruits()->getIndex() > 15) {
                CCDirector::sharedDirector()->replaceScene(GameResultScene::scene(kResultFinished));
            }else {
                CCDirector::sharedDirector()->replaceScene(GameMemoryScene::scene());
            }
        }
            break;
        case kResultFailed:
        {
            if (CCFruitsData::sharedFruits()->getChanceNum() <= 0) {
                CCFruitsData::sharedFruits()->clearFruits();
                CCFruitsData::sharedFruits()->getFruits()->removeAllObjects();
                CCDirector::sharedDirector()->replaceScene(HelloWorld::scene());
            }else {
                CCFruitsData::sharedFruits()->indexAdd();
                CCDirector::sharedDirector()->replaceScene(GameMemoryScene::scene());
            }
        }
            break;
        default:
            break;
    }
}

void GameResultScene::goToStore() {
    CCScene *scene = CCScene::create();
    GameShopScene *layer = new GameShopScene();
    layer->init(GameShopScene::kBackResultMenuType);
    layer->setResultType(result);
    scene->addChild(layer);
    CCDirector::sharedDirector()->replaceScene(scene);
}

void GameResultScene::goToMainMenu() {
    CCDirector::sharedDirector()->replaceScene(HelloWorld::scene());
}





