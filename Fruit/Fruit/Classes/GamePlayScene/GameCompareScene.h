//
//  GameCompareScene.h
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#ifndef __Fruit__GameCompareScene__
#define __Fruit__GameCompareScene__

#include <iostream>
#include "cocos2d.h"
#include "FruitSprite.h"

class GameCompareScene: public cocos2d::CCLayer {
public:
    ~GameCompareScene() {
        CC_SAFE_RELEASE(spriteArray);
    }
    bool init(cocos2d::CCArray *array);
    static cocos2d::CCScene *scene(cocos2d::CCArray *array);
private:
    cocos2d::CCPoint getLeftPointFromFramePoint(int framePoint);
    cocos2d::CCPoint getRightPointFromFramePoint(int framePoint);
    void animationCallBack(cocos2d::CCNode *pNode,void *data);
    void animationCallAct(cocos2d::CCNode *pNode);
    void result(bool result);
    void addWrong(FruitSprite *sprite);
    bool isSucess;
    cocos2d::CCArray *spriteArray;
};

#endif /* defined(__Fruit__GameCompareScene__) */
