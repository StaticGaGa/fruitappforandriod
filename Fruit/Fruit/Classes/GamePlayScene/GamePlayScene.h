//
//  GamePlayScene.h
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#ifndef __Fruit__GamePlayScene__
#define __Fruit__GamePlayScene__

#include <iostream>
#include "cocos2d.h"
#include "FruitSprite.h"

class GamePlayScene: public cocos2d::CCLayer {
public:
    ~GamePlayScene() {
        CC_SAFE_RELEASE(putArray);
        CC_SAFE_RELEASE(showArray);
    }
    CREATE_FUNC(GamePlayScene);
    bool init();
    static cocos2d::CCScene *scene();
    
    virtual void ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);
    virtual void ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);
    virtual void ccTouchesEnded(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);
    
private:
    void itemClicked(CCObject *pObj);
    cocos2d::CCPoint getPointFromFramePoint(int framePoint);
    cocos2d::CCPoint getRightPointFromFramePoint(int framePoint);
    cocos2d::CCPoint getLeftPointFromFramePoint(int framePoint);
    cocos2d::CCArray *showArray;
    FruitSprite *moveSprite;
    cocos2d::CCArray *putArray;
    cocos2d::CCPoint beginPoint;
    void setSprite(FruitSprite *tmpMoveSprite, int tmpFramPoint, bool act);
    void animationCallBack(CCNode *pNode);
    void helpClicked(CCNode *node);
    FruitSprite *findFruitSprite(FruitSprite *sprite);
    void fruitsMoveAway();
    void finishMenu();
    void goNext();
};
#endif /* defined(__Fruit__GamePlayScene__) */
