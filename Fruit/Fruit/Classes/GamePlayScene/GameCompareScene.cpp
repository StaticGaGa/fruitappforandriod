//
//  GameCompareScene.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#include "GameCompareScene.h"
#include "CCFruitsData.h"
#include "GameResultScene.h"

#define Background "Memory_Background.png"
#define Frame_Left "Compare_Frame_Left.png"
#define Frame_Right "Compare_Frame_Right.png"
#define Wrong "wrong.png"
#define Star_Background "compareBG.png"

#define Chance_Num 5
#define Left_Frame_Tag 1
#define Right_Frame_Tag 2
#define Star_Background_Tag 3

#define Scene_Change_Time 2.5
#define SpriteHead "compareHead.png"

using namespace cocos2d;

CCScene *GameCompareScene::scene(CCArray *arrray) {
    CCScene *scene = CCScene::create();
    GameCompareScene *layer = new GameCompareScene();
    layer->init(arrray);
    scene->addChild(layer);
    layer->autorelease();
    return scene;
}

bool GameCompareScene::init(CCArray *array) {
    if (!CCLayer::init()) {
        return false;
    }
    
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    CCSprite *bg = CCSprite::create(Background);
    bg->setPosition(ccp(winSize.width/2,winSize.height/2));
    this->addChild(bg);
    
    CCSprite *rightFrame = CCSprite::create(Frame_Right);
    CCSize cttSize = rightFrame->getContentSize();
    rightFrame->setPosition(ccp(winSize.width - cttSize.width/2, winSize.height/2));
    rightFrame->setTag(Right_Frame_Tag);
    this->addChild(rightFrame);
    
    CCSprite *leftFrame = CCSprite::create(Frame_Left);
    cttSize = leftFrame->getContentSize();
    leftFrame->setPosition(ccp(cttSize.width/2, winSize.height/2));
    leftFrame->setTag(Left_Frame_Tag);
    this->addChild(leftFrame);
    
    CCSprite *starBg = CCSprite::create(Star_Background);
    cttSize = starBg->getContentSize();
    starBg->setPosition(ccp(winSize.width - cttSize.width, winSize.height/2));
    starBg->setTag(Star_Background_Tag);
    this->addChild(starBg);
    spriteArray = new CCArray();
    for (int num = 0; num < Chance_Num; num++) {
        CCSprite *sprite = CCSprite::create(SpriteHead);
        sprite->setPosition(ccp(cttSize.width/2, cttSize.height/10*(8 - num*2 + 1)));
        sprite->setTag(num);
        starBg->addChild(sprite);
        spriteArray->addObject(sprite);
    }
    
    CCArray *tmpArray = CCFruitsData::sharedFruits()->getFruits();
    int count = tmpArray->count();
    for (int num = 0; num < count; num++) {
        FruitSprite *sprite = (FruitSprite *)tmpArray->objectAtIndex(num);
        FruitSprite *neSprite = (FruitSprite *)CCSprite::createWithTexture(sprite->getTexture());
        neSprite->setFramePoint(sprite->getFramePoint());
        neSprite->setPosition(this->getLeftPointFromFramePoint(neSprite->getFramePoint()));
        leftFrame->addChild(neSprite);
    }
    
    isSucess = true;
    if (tmpArray->count() != array->count()) {
        isSucess = false;
    }
    
    count = array->count();
    if (0 == count) {
        for (int i = 0; i < tmpArray->count();i++) {
            CCFruitsData::sharedFruits()->chanceNumSub();
        }
    }else {
        for (int num = 0; num < count;num++) {
            FruitSprite *sprite = (FruitSprite *)array->objectAtIndex(num);
            FruitSprite *neSprite = (FruitSprite *)CCSprite::createWithTexture(sprite->getTexture());
            neSprite->setFramePoint(sprite->getFramePoint());
            neSprite->setPosition(this->getRightPointFromFramePoint(neSprite->getFramePoint()));
            rightFrame->addChild(neSprite);
            
            if (!CCFruitsData::sharedFruits()->fruitsCompare(neSprite)) {
                CCFruitsData::sharedFruits()->chanceNumSub();
                this->addWrong(neSprite);
                isSucess = false;
            }
        }
    }
    
    for (int num = 0 ; num < Chance_Num;num++) {
        CCNode *node = starBg->getChildByTag(num);
        CCDelayTime *delayAct = CCDelayTime::create((num + 1)/2);
//        CCCallFuncN *callAct = CCCallFuncN::create(this, callfuncN_selector(GameCompareScene::animationCallBack));
        CCCallFuncND *callAc = CCCallFuncND::create(this, callfuncND_selector(GameCompareScene::animationCallBack), node);
       this->runAction(CCSequence::create(delayAct,callAc,NULL));
    }
    
    CCDelayTime *delayAct = CCDelayTime::create(Scene_Change_Time);
    CCCallFuncN *callAct = CCCallFuncN::create(this, callfuncN_selector(GameCompareScene::animationCallAct));
    this->runAction(CCSequence::create(delayAct,callAct,NULL));
    return true;
}

void GameCompareScene::animationCallBack(cocos2d::CCNode *pNode,void *data) {
//    if (data) {
        CCNode *node = (CCNode *)data;
        node->removeFromParentAndCleanup(true);
//    }
}

void GameCompareScene::animationCallAct(cocos2d::CCNode *pNode) {
    this->result(isSucess);
}

void GameCompareScene::addWrong(FruitSprite *sprite) {
    CCSize cttSize = sprite->getContentSize();
    CCSprite *wrong = CCSprite::create(Wrong);
    wrong->setPosition(ccp(cttSize.width/2, cttSize.height/2));
    sprite->addChild(wrong);
}

void GameCompareScene::result(bool result) {
    if (result) {
        CCDirector::sharedDirector()->replaceScene(GameResultScene::scene(GameResultScene::kResultSuccess));
    }else {
        CCDirector::sharedDirector()->replaceScene(GameResultScene::scene(GameResultScene::kResultFailed));

    }
}

CCPoint GameCompareScene::getLeftPointFromFramePoint(int framePoint) {
    CCSprite *frameSprite = (CCSprite *)this->getChildByTag(Left_Frame_Tag);
    CCSize cttSize = frameSprite->getContentSize();
    CCPoint point;
    switch(framePoint)
    {
        case 0:
        {
            point = CCPointMake(cttSize.width/20*5, cttSize.height/80*16);
            break;
        }
        case 1:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*16);
            break;
        }
        case 2:
        {
            point = CCPointMake(cttSize.width/20*13, cttSize.height/80*16);
            break;
        }
        case 3:
        {
            point = CCPointMake(cttSize.width/20*5, cttSize.height/80*28);
            break;
        }
        case 4:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*28);
            break;
        }
        case 5:
        {
            point = CCPointMake(cttSize.width/20*13, cttSize.height/80*28);
            break;
        }
        case 6:
        {
            point = CCPointMake(cttSize.width/20*5, cttSize.height/80*41);
            break;
        }
        case 7:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*41);
            break;
        }
        case 8:
        {
            point = CCPointMake(cttSize.width/20*13, cttSize.height/80*41);
            break;
        }
        case 9:
        {
            point = CCPointMake(cttSize.width/20*5, cttSize.height/80*53);
            break;
        }
        case 10:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*53);
            break;
        }
        case 11:
        {
            point = CCPointMake(cttSize.width/20*13, cttSize.height/80*53);
            break;
        }
        case 12:
        {
            point = CCPointMake(cttSize.width/20*5, cttSize.height/80*66);
            break;
        }
        case 13:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*66);
            break;
        }
        case 14:
        {
            point = CCPointMake(cttSize.width/20*13, cttSize.height/80*66);
            break;
        }
    }
    return point;

}

CCPoint GameCompareScene::getRightPointFromFramePoint(int framePoint) {
    CCSprite *frameSprite = (CCSprite *)this->getChildByTag(Right_Frame_Tag);
    CCSize cttSize = frameSprite->getContentSize();
    CCPoint point;
    switch(framePoint)
    {
        case 0:
        {
            point = CCPointMake(cttSize.width/40*11, cttSize.height/80*16);
            break;
        }
        case 1:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*16);
            break;
        }
        case 2:
        {
            point = CCPointMake(cttSize.width/40*25, cttSize.height/80*16);
            break;
        }
        case 3:
        {
            point = CCPointMake(cttSize.width/40*11, cttSize.height/80*28);
            break;
        }
        case 4:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*28);
            break;
        }
        case 5:
        {
            point = CCPointMake(cttSize.width/40*25, cttSize.height/80*28);
            break;
        }
        case 6:
        {
            point = CCPointMake(cttSize.width/40*11, cttSize.height/80*41);
            break;
        }
        case 7:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*41);
            break;
        }
        case 8:
        {
            point = CCPointMake(cttSize.width/40*25, cttSize.height/80*41);
            break;
        }
        case 9:
        {
            point = CCPointMake(cttSize.width/40*11, cttSize.height/80*54);
            break;
        }
        case 10:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*54);
            break;
        }
        case 11:
        {
            point = CCPointMake(cttSize.width/40*25, cttSize.height/80*54);
            break;
        }
        case 12:
        {
            point = CCPointMake(cttSize.width/40*11, cttSize.height/80*67);
            break;
        }
        case 13:
        {
            point = CCPointMake(cttSize.width/20*9, cttSize.height/80*67);
            break;
        }
        case 14:
        {
            point = CCPointMake(cttSize.width/40*25, cttSize.height/80*67);
            break;
        }
    }
    return point;

}



