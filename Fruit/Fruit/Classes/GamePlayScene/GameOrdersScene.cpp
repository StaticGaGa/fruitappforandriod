//
//  GameOrdersScene.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-16.
//
//

#include "GameOrdersScene.h"
#include "GameMemoryScene.h"

#define Background "youxiyemianbeijing.png"
//sprites
#define Salesperson "youxijiemiandandingdang.png"
#define Customer "youxijiemianshengdane.png"
#define NextStep "duihuayexiayibu.png"

#define kTag_Dialogue 1001
#define kTag_Customer 1002
#define kTag_NextStep 1003

#define Move_Time 1.5
#define Scale_Time  0.5

using namespace cocos2d;

CCScene *GameOrdersScene::scene()
{
    GameOrdersScene *layer = GameOrdersScene::create();
    CCScene *scene = CCScene::create();
    scene->addChild(layer);
    return scene;
}

bool GameOrdersScene::init() {
    if (!CCLayer::init()) {
        return false;
    }
    totoalTouches = 0;
    this->setTouchEnabled(true);
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    CCSprite *bg = CCSprite::create(Background);
    bg->setPosition(ccp(winSize.width/2, winSize.height/2));
    this->addChild(bg);
    
    CCSprite *salesSprite = CCSprite::create(Salesperson);
    CCSize salesContentSize = salesSprite->getContentSize();
    salesSprite->setPosition(ccp(winSize.width/3, winSize.height/3));
    this->addChild(salesSprite);
    
    CCSprite *customerSprite = CCSprite::create(Customer);
    CCSize custContentSize = customerSprite->getContentSize();
    customerSprite->setPosition(ccp(winSize.width + custContentSize.width/2, winSize.height/3));
    customerSprite->setTag(kTag_Customer);
    this->addChild(customerSprite);
    
    CCSprite *dialogueSprite = CCSprite::create("duihua1.png");
    dialogueSprite->setPosition(ccp(winSize.width/2, winSize.height - dialogueSprite->getContentSize().width/2));
    dialogueSprite->setScale(0.0);
    dialogueSprite->setTag(kTag_Dialogue);
    this->addChild(dialogueSprite);
    
    CCSprite *normalSpr = CCSprite::create(NextStep);
    CCMenuItemSprite *nextMenu = CCMenuItemSprite::create(normalSpr, NULL, this, menu_selector(GameOrdersScene::goNext));
    nextMenu->setPosition(ccp(winSize.width - normalSpr->getContentSize().width, normalSpr->getContentSize().height));
    nextMenu->setRotation(0.0);
    CCMenu *menu = CCMenu::create(nextMenu,NULL);
    menu->setPosition(CCPointZero);
    this->addChild(menu);
    
    CCRotateBy *rote1 = CCRotateBy::create(0.7, 360*2);
    CCRotateBy *rote2 = CCRotateBy::create(0.8, 360);
    nextMenu->runAction(CCSequence::create(rote1,rote2,NULL));
    
    CCCallFuncN *callBack = CCCallFuncN::create(this, callfuncN_selector(GameOrdersScene::animationCallback));
    CCMoveBy *move = CCMoveBy::create(0.5,ccp(-custContentSize.width*10/9 + 30, 0));
    CCMoveBy *move2 = CCMoveBy::create(0.1, ccp(-30, 0));
    customerSprite->runAction(CCSequence::create(move,move2,callBack,NULL));
    
    return true;
}

void GameOrdersScene::goNext(cocos2d::CCObject *psender)
{
    CCDirector::sharedDirector()->replaceScene(GameMemoryScene::scene());
}

void GameOrdersScene::animationCallback(cocos2d::CCNode *psender)
{
    if (NULL == psender) {
        return;
    }
    
    int tag = psender->getTag();
    switch (tag) {
        case kTag_Customer:
        {
            CCSprite *dialogueSprite = (CCSprite *)this->getChildByTag(kTag_Dialogue);
            CCScaleTo *scale = CCScaleTo::create(Scale_Time, 1.0);
            CCCallFuncN *callBack = CCCallFuncN::create(this, callfuncN_selector(GameOrdersScene::animationCallback));
            dialogueSprite->runAction(CCSequence::create(scale,callBack,NULL));
        }
            break;
        case kTag_Dialogue:
        {
            dialogueFinished = true;
        }
            break;
        default:
            break;
    }
}

void GameOrdersScene::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 0, true);
}

bool GameOrdersScene::ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    if (dialogueFinished) {
        dialogueFinished = !dialogueFinished;
        totoalTouches++;
        switch (totoalTouches) {
            case 1:
            {
                //出对话2
                this->showDialogue2();
            }
                break;
            case 2:
            {
                //出对话三
                this->showDialogue3();
            }
                break;
            default:
                break;
        }
    }
    
    return true;
}

void GameOrdersScene::showDialogue2()
{
    CCSprite *sprite = (CCSprite *)this->getChildByTag(kTag_Dialogue);
    if (sprite) {
        sprite->removeFromParentAndCleanup(true);
    }
    
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    CCSprite *dialogueSprite = CCSprite::create("duihua2.png");
    dialogueSprite->setTag(kTag_Dialogue);
    dialogueSprite->setScale(0.0);
    dialogueSprite->setPosition(ccp(winSize.width - dialogueSprite->getContentSize().width/2,winSize.height - dialogueSprite->getContentSize().height*0.7));
    this->addChild(dialogueSprite);
    
    CCScaleTo *scale = CCScaleTo::create(Scale_Time, 1.0);
    CCCallFuncN *callBack = CCCallFuncN::create(this, callfuncN_selector(GameOrdersScene::animationCallback));
    dialogueSprite->runAction(CCSequence::create(scale,callBack,NULL));
}

void GameOrdersScene::showDialogue3()
{
    CCSprite *sprite = (CCSprite *)this->getChildByTag(kTag_Dialogue);
    if (sprite) {
        sprite->removeFromParentAndCleanup(true);
    }
    
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    CCSprite *dialogueSprite = CCSprite::create("duihua3.png");
    dialogueSprite->setTag(kTag_Dialogue);
    dialogueSprite->setScale(0.0);
    dialogueSprite->setPosition(ccp(winSize.width/2, winSize.height - dialogueSprite->getContentSize().width/2));
    this->addChild(dialogueSprite);
    
    CCScaleTo *scale = CCScaleTo::create(Scale_Time, 1.0);
    CCCallFuncN *callBack = CCCallFuncN::create(this, callfuncN_selector(GameOrdersScene::animationCallback));
    dialogueSprite->runAction(CCSequence::create(scale,callBack,NULL));
}

