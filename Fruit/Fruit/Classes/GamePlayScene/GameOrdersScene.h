//
//  GameOrdersScene.h
//  Fruit
//
//  Created by Static Ga on 13-5-16.
//
//

#ifndef __Fruit__GameOrdersScene__
#define __Fruit__GameOrdersScene__

#include <iostream>
#include "cocos2d.h"
class GameOrdersScene : public cocos2d::CCLayer {
public:
    CREATE_FUNC(GameOrdersScene);
    static cocos2d::CCScene *scene();
    virtual bool init();
    virtual void registerWithTouchDispatcher(void);
    virtual bool ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
    void animationCallback(CCNode *psender);
    void goNext(CCObject *psender);
private:
    int totoalTouches;
    bool dialogueFinished;
    
    void showDialogue2();
    void showDialogue3();
};
#endif /* defined(__Fruit__GameOrdersScene__) */
