//
//  GameResultScene.h
//  Fruit
//
//  Created by Static Ga on 13-5-20.
//
//

#ifndef __Fruit__GameResultScene__
#define __Fruit__GameResultScene__

#include <iostream>
#include "cocos2d.h"
class GameResultScene: public cocos2d::CCLayer {
public:
    typedef enum {
        kResultSuccess = 0,//本局成功
        kResultFailed,//本局失败
        kResultFinished,//整个游戏通关
    }kResultType;
    static cocos2d::CCScene *scene(int theResult);
    bool init(int result);
    void itemClicked(cocos2d::CCObject *pObj);
    void setUI();
private:
    kResultType result;
    void goOnGame();
    void goToStore();
    void goToMainMenu();
};
#endif /* defined(__Fruit__GameResultScene__) */
