#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "GameDeveloperScene.h"
#include "GameShopScene.h"
#include "GameSinaLayer.h"
#include "GameOrdersScene.h"
#include "GameLoginAwardsScene.h"
#include "CCPlistData.h"

#define Background "GameMenu_Background.png"

#define Play_Menu "Play_Menu.png"
#define Play_Menu_Selected "Play_Menu2.png"
#define Center_Menu "reset.png"
#define Center_Menu_Selected "resetPressed.png"
#define Setting_Menu "Setting_Menu.png"
#define Setting_Menu_Selected "Setting_Menu2.png"
#define Shop_Menu "Shop_Menu.png"
#define Shop_Menu_Selected "Shop_Menu2.png"
#define Sina_Menu "Sina_Menu.png"
#define Sina_Menu_Selected "Sina_Menu2.png"

#define Game_SoundOn "soundON.png"
#define Game_SoundOff "soundOFF.png"
#define Game_DeveloperNormal "developer01.png"
#define Game_DeveloperPressed "developer02.png"
#define Game_RankingNormal "ranking01.png"
#define Game_RankingPressed "ranking02.png"
#define Game_AchieveNormal "achivements01.png"
#define Game_AchievePressed "achivements02.png"
#define Game_IconContainer "iconBounder.png"

#define Tag_ShareSinaBg 1001
#define Tag_ShareSprite 1002
#define Tag_ShareBack 1003

#define Tag_SetMenu 1004
#define Tag_Sound 1005
#define Tag_Developer 1006

#define Tag_GameCenterMenu 1007
#define Tag_Rank 1008
#define Tag_Achieve 1009

#define Tag_ShareMenu 1010
#define Tag_Menu 1011
#define Tag_IconContainer 1012


using namespace cocos2d;
using namespace CocosDenshion;

CCScene* HelloWorld::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    HelloWorld *layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}
void test () {
    cc_timeval *startTime=new cc_timeval;
    CCTime::gettimeofdayCocos2d(startTime,NULL);
    cc_timeval *endTime=new cc_timeval;
    CCTime::gettimeofdayCocos2d(endTime,NULL);
    double duration=CCTime::timersubCocos2d(startTime,endTime);
    printf("duration=%f\n",duration/1000);
    printf("start %ld %ld",startTime->tv_sec/1000,startTime->tv_usec);
    delete startTime;
    delete endTime;
}
// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
   // test();
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    CCSprite *backgroundSprite = CCSprite::create(Background);
    backgroundSprite->setPosition(ccp(winSize.width/2, winSize.height/2));
    this->addChild(backgroundSprite);
    
    CCSprite *normalSprite = CCSprite::create(Setting_Menu);
    CCSprite *selectedSprite = CCSprite::create(Setting_Menu_Selected);
    
    CCMenuItemSprite *settingItem = CCMenuItemSprite::create(normalSprite, selectedSprite, this, menu_selector(HelloWorld::setItemClicked));
    settingItem->setPosition(ccp(winSize.width/7*6, winSize.height/8*7));
    
    normalSprite = CCSprite::create(Shop_Menu);
    selectedSprite = CCSprite::create(Shop_Menu_Selected);
    
    CCMenuItemSprite *shopItem = CCMenuItemSprite::create(normalSprite, selectedSprite, this, menu_selector(HelloWorld::shopItemClicked));
    shopItem->setPosition(ccp(winSize.width/8*7, winSize.height/7*5));

    normalSprite = CCSprite::create(Sina_Menu);
    selectedSprite = CCSprite::create(Sina_Menu_Selected);
    
    CCMenuItemSprite *sinaItem = CCMenuItemSprite::create(normalSprite, selectedSprite, this, menu_selector(HelloWorld::sinaItemClicked));
    sinaItem->setPosition(ccp(winSize.width/28*25, winSize.height/56*31));
    
    normalSprite = CCSprite::create(Play_Menu);
    selectedSprite = CCSprite::create(Play_Menu_Selected);
    
    CCMenuItemSprite *playItem = CCMenuItemSprite::create(normalSprite, selectedSprite, this, menu_selector(HelloWorld::playItemClicked));
    playItem->setPosition(ccp(winSize.width/5*4, winSize.height/7));

    CCMenu *menu = CCMenu::create(settingItem,shopItem,sinaItem,playItem,NULL);
    menu->setPosition(CCPointZero);
    menu->setTag(Tag_Menu);
    this->addChild(menu);
    
    normalSprite = CCSprite::create(Game_SoundOn);
    selectedSprite = CCSprite::create(Game_SoundOff);
    
    CCMenuItemSprite *soundItem = CCMenuItemSprite::create(normalSprite, selectedSprite, this, menu_selector(HelloWorld::soundItemClicked));
    soundItem->setTag(Tag_Sound);
    soundItem->setPosition(settingItem->getPosition());
    
    normalSprite = CCSprite::create(Game_DeveloperNormal);
    selectedSprite = CCSprite::create(Game_DeveloperPressed);

    CCMenuItemSprite *developerItem = CCMenuItemSprite::create(normalSprite, selectedSprite, this, menu_selector(HelloWorld::developerItemClicked));
    developerItem->setTag(Tag_Developer);
    developerItem->setPosition(settingItem->getPosition());
    
    CCMenu *settingMenu = CCMenu::create(soundItem,developerItem,NULL);
    settingMenu->setPosition(CCPointZero);
    settingMenu->runAction(CCHide::create());
    settingMenu->setEnabled(false);
    settingMenu->setTag(Tag_SetMenu);
    this->addChild(settingMenu);
    this->judgeLoginAwards();
    return true;
}

//ItemMethods
void HelloWorld::setItemClicked(CCObject *pSender)
{
    int num = 0;
       
    CCMenu *settingMenu = (CCMenu *)this->getChildByTag(Tag_SetMenu);
    CCAction *menuAction = NULL;
    if (didShowSubItem) {
        menuAction = CCHide::create();
        settingMenu->setEnabled(false);
        num = 1;
    }else {
        menuAction = CCShow::create();
        settingMenu->setEnabled(true);
        num = -1;
    }
    didShowSubItem = !didShowSubItem;

    settingMenu->runAction(CCShow::create());
    CCMenuItemSprite *developerItem = (CCMenuItemSprite *)settingMenu->getChildByTag(Tag_Developer);
    CCMenuItemSprite *soundItem = (CCMenuItemSprite *)settingMenu->getChildByTag(Tag_Sound);
    
    CCMoveBy *developerMove = CCMoveBy::create(0.5, ccp(num*developerItem->getContentSize().width*2, 0));
    CCMoveBy *soundMove = CCMoveBy::create(0.5, ccp(num*soundItem->getContentSize().width, 0));
    developerItem->runAction(developerMove);
    soundItem->runAction(soundMove);
    
    settingMenu->runAction(CCSequence::create(CCDelayTime::create(0.5),menuAction,NULL));
}

void HelloWorld::sinaItemClicked(cocos2d::CCObject *pSender)
{
    GameSinaLayer *sinaLayer = GameSinaLayer::create();
    this->addChild(sinaLayer);
}

void HelloWorld::shopItemClicked(cocos2d::CCObject *pSender)
{
    CCDirector::sharedDirector()->replaceScene(GameShopScene::sceneWithType(GameShopScene::kBackMainMenuType));
}

void HelloWorld::playItemClicked(cocos2d::CCObject *pSender)
{
    CCDirector::sharedDirector()->replaceScene(GameOrdersScene::scene());
}

void HelloWorld::soundItemClicked(cocos2d::CCObject *pSender)
{
    
}

void HelloWorld::developerItemClicked(cocos2d::CCObject *pSender)
{
    CCDirector::sharedDirector()->replaceScene(GameDeveloperScene::scene());
}

void HelloWorld::menuCloseCallback(CCObject* pSender)
{
    CCDirector::sharedDirector()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void HelloWorld::judgeLoginAwards() {
    if (CCPlistData::shouldShowLoginAwards()) {
        this->addChild(GameLoginAwardsScene::create());
    }
}
