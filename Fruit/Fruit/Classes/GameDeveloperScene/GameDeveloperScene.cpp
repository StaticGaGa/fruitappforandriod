//
//  GameDeveloperScene.cpp
//  Fruit
//
//  Created by Static Ga on 13-5-9.
//
//

#include "GameDeveloperScene.h"
#include "HelloWorldScene.h"

#define R_GameFinishedBG "tongyongjiemian03.png"
#define R_GoBack "fanhui.png"

using namespace cocos2d;

CCScene *GameDeveloperScene::scene()
{
    CCScene *scene = CCScene::create();
    GameDeveloperScene *layer = GameDeveloperScene::create();
    scene->addChild(layer);
    return scene;
}

bool GameDeveloperScene::init()
{
    if (!CCLayer::init()) {
        return false;
    }
    
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    CCSprite *bgSprite = CCSprite::create(R_GameFinishedBG);
    bgSprite->setPosition(ccp(winSize.width/2, winSize.height/2));
    this->addChild(bgSprite);
    
    CCSprite *normalSprite = CCSprite::create(R_GoBack);
    CCMenuItemSprite *gobackItem = CCMenuItemSprite::create(normalSprite, NULL, this, menu_selector(GameDeveloperScene::goBack));
    gobackItem->setPosition(CCPointMake(winSize.width - gobackItem->getContentSize().width*2, gobackItem->getContentSize().height));
    CCMenu *menu = CCMenu::create(gobackItem,NULL);
    menu->setPosition(CCPointZero);
    this->addChild(menu);
    return true;
}

void GameDeveloperScene::goBack(cocos2d::CCObject *pSender)
{
    CCDirector::sharedDirector()->replaceScene(HelloWorld::scene());
}