//
//  GameDeveloperScene.h
//  Fruit
//
//  Created by Static Ga on 13-5-9.
//
//

#ifndef __Fruit__GameDeveloperScene__
#define __Fruit__GameDeveloperScene__

#include <iostream>
#include "cocos2d.h"
class GameDeveloperScene: public cocos2d::CCLayer
{
public:
    static cocos2d::CCScene *scene();
    bool init();
    CREATE_FUNC(GameDeveloperScene);
private:
    void goBack(CCObject *pSender);
};
#endif /* defined(__Fruit__GameDeveloperScene__) */
